var indexSectionsWithContent =
{
  0: "abcgijmopstx",
  1: "abcgm",
  2: "jx",
  3: "acgimos",
  4: "t",
  5: "p"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Events"
};

