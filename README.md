# README #

Egyetemi programozás 3 tárgy kötelező beadandója egy játék készítése volt. Az alábbi feltételeknek kellett megfelelnie:

	- Egyszerű felhasználói interakcióval (egérrel/billentyűzettel) vezérelhető
	logikai/ügyességi játék.
	- Teljesíti a rétegek szétválasztásának alapelvét (üzleti logika leválasztása[1], 
	valamint a WPF kód MVVM elvű szétválasztása[2]).
	- Megjelenítés DrawingImage-ek / FrameworkElement segítségével.
	- Játékfejlesztő framework (pl. XNA, Unity) nem használható!
	- Megjelenítési framework (pl. SDL, DirectX, OpenGL, Vulcan) használható, de az
	emiatt a megjelenítésre fordítandó és a keretrendszerrel megspórolt időt a játék
	logikájára kell ráfordítani!
	
A játékom egyszerű, billentyűzet vezérlésű ügyességi játék. A saját karaktert kell eljuttatni a megadott megadott pontból
a kijelölt célig a mozgó vagy statikus akadályokat kikerülve. 

A játék 9 darab pályát/levelt tartalmaz. Egy pálya teljesítése után a játék automatikusan ugrik a következőre. A játék
megjelenítését DrawingImage-ek segítségével oldottam meg. A pályák txt fájlból kerülnek beolvasásra. Minden megjelenítendő 
karakter küllönböző betű jelöli a szövegfájlban. Beolvasás után mindegyik eltárolódik a megfelelő tömbben, listában (a pályát
körbevevő falak, statikus ellenségek boolean tömbben, míg a mozgó ellenségek ObservableCollection-ekben a mozgásuk lekövetéseképpen). Konvertelek segítségével 
kerülnek kirajzolásra.

A különböző ellenségek: 
	- horizontális mozgású állat
	- vertikális mozgású állat
	- statikus egyhelyben található akadály
	- pattogó mozgást utánzó ellenség: diagonálisan mozog, mely falhoz érve visszapattan.
	- a saját karakter mozgását utánzó, kis késleltetéssel lekövető állat


