﻿// <copyright file="EnemiesToDrawingConverter.cs" company="OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Jatek
{
    using System;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// The class to convert from gameitem to drawing the list of enemies on which it has been called
    /// </summary>
    internal class EnemiesToDrawingConverter : IValueConverter
    {
        /// <summary>
        /// Converting from gameitem to drawing
        /// </summary>
        /// <param name="value">ObservableCollection of GameItems</param>
        /// <param name="targetType">geometrygroup</param>
        /// <param name="parameter">Converter parameter to use</param>
        /// <param name="culture">Culture to use</param>
        /// <returns>the geometrygroup containing enemies as rectanglegeometries</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ObservableCollection<GameItem> enemies = value as ObservableCollection<GameItem>;

            GeometryGroup geometryGroup = new GeometryGroup();

            for (int i = 0; i < enemies.Count; i++)
            {
                Rect r = new Rect(enemies[i].X * Constants.TILEW, enemies[i].Y * Constants.TILEH, Constants.TILEW, Constants.TILEH);
                geometryGroup.Children.Add(new RectangleGeometry(r));
            }

            return geometryGroup;
        }

        /// <summary>
        /// No conversion back here
        /// </summary>
        /// <param name="value">GeometryGroup of rectangleGeometries</param>
        /// <param name="targetType">ObservableCollection of appropriate enemies</param>
        /// <param name="parameter">Converter paramater to use</param>
        /// <param name="culture">Culture to use</param>
        /// <returns>ObservableCollection</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}