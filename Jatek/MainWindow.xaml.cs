﻿// <copyright file="MainWindow.xaml.cs" company="OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Jatek
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Private variable to back storage of gamelogic
        /// </summary>
        private GameLogic logic;

        /// <summary>
        /// Private variable of the timer
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// Private variable for level counting
        /// </summary>
        private int levelNumber;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Method for user interaction
        /// </summary>
        /// <param name="sender">Keyboard hit that has raised the event</param>
        /// <param name="e">Parameter e contains the event data</param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            bool isOver = false;

            switch (e.Key)
            {
                case Key.Up: isOver = this.logic.MouseMove(0, -1); break;
                case Key.Down: isOver = this.logic.MouseMove(0, 1); break;
                case Key.Right: isOver = this.logic.MouseMove(1, 0); break;
                case Key.Left: isOver = this.logic.MouseMove(-1, 0); break;
            }

            if (this.logic.CatchedMouse())
            {
                this.EndGame("The game is over! You have been catched");
            }

            if (isOver)
            {
                if (this.levelNumber == 9)
                {
                    this.EndGame("The end!");
                }
                else
                {
                    // Increase the levels --> load the next field with the help of levelcounter
                    // with creating a new logic
                    this.logic = new GameLogic(++this.levelNumber);

                    // Subscribe to the event
                    this.logic.EnemyHit += this.Logic_EnemyHit;
                    this.DataContext = this.logic.Model;

                    // the movement of the enemies is timer dependant --> stop the timer and then restart it
                    this.timer.Stop();
                    this.timer.Start();
                }
            }
        }

        /// <summary>
        /// Method responsible for loading the window
        /// </summary>
        /// <param name="sender">Parameter that has raised the event</param>
        /// <param name="e">Parameter e the contains event data</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.levelNumber = 7;
            this.logic = new GameLogic(this.levelNumber);

            this.DataContext = this.logic.Model;

            //if (this.logic.Model.EnemiesGiven)
            //{
                this.logic.EnemyHit += this.Logic_EnemyHit;

                this.timer = new DispatcherTimer();
                this.timer.Interval = TimeSpan.FromMilliseconds(400);
                this.timer.Tick += this.Timer_Tick;
                this.timer.Start();
            //}
        }

        /// <summary>
        /// The method that has suscribed for the event.
        /// </summary>
        /// <param name="source">The appropriate GameItem, matching parameter</param>
        private void Logic_EnemyHit(GameItem source)
        {
            // Unscribe from the event
            this.logic.EnemyHit -= this.Logic_EnemyHit;

            // End the game
            this.EndGame("The end!");
        }

        /// <summary>
        /// Method that has subscribed for the timer tick, thus move the gameitems for every time tick
        /// </summary>
        /// <param name="sender">Parameter that has raised the event</param>
        /// <param name="e">Parameter e that contains event data</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            this.logic.MoveEnemies();
        }

        /// <summary>
        /// Method that is called when the game ends, thus avoiding duplication
        /// </summary>
        /// <param name="dialogText">String that should be written out</param>
        private void EndGame(string dialogText)
        {
            // Stop the timer, end the game, and finally clos the window
            this.timer.Stop();
            MessageBox.Show(dialogText);
            this.Close();
        }
    }
}
