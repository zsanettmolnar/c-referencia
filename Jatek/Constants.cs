﻿// <copyright file="Constants.cs" company="OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Jatek
{
    /// <summary>
    /// an enumeration of values, the dimensions of the gameitems, that are constants for the whole game
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// Constant that stores the width of every item
        /// </summary>
        public const int TILEW = 40;

        /// <summary>
        /// Constant that stores the height of every item
        /// </summary>
        public const int TILEH = 40;
    }
}
