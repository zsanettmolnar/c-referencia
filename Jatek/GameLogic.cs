﻿// <copyright file="GameLogic.cs" company="OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Jatek
{
    using System;
    using System.IO;
    using System.Windows;

    /// <summary>
    /// Class for describing the logic of the game (movements, rules, reading the levels)
    /// </summary>
    internal class GameLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// Read the appropriate level text, and create instance of the appropriate gameitems
        /// </summary>
        /// <param name="levelNumber">gets the appropriate levelnumber as a paramater</param>
        public GameLogic(int levelNumber)
        {
            string fname = levelNumber.ToString() + ".txt";
            this.Model = new GameModel();
            this.Model.LevelNumber = levelNumber;
            string[] lines = File.ReadAllLines(fname);
            int xsize = int.Parse(lines[0]);
            int ysize = int.Parse(lines[1]);

            this.Model.Walls = new bool[xsize, ysize];
            this.Model.MouseCatchers = new bool[xsize, ysize];

            for (int x = 0; x < xsize; x++)
            {
                for (int y = 0; y < ysize; y++)
                {
                    this.Model.Walls[x, y] = lines[y + 2][x] == 'w';
                    this.Model.MouseCatchers[x, y] = lines[y + 2][x] == 'x';

                    if (lines[y + 2][x] == 'm')
                    {
                        this.Model.Mouse = new Point(x, y);
                    }

                    if (lines[y + 2][x] == 'c')
                    {
                        this.Model.Cheese = new Point(x, y);
                    }

                    if (lines[y + 2][x] == 'b')
                    {
                        this.Model.BouncingEnemies.Add(new GameItem(x, y));
                        this.Model.EnemiesGiven = true;
                    }

                    if (lines[y + 2][x] == 'h')
                    {
                        this.Model.HorizontalEnemies.Add(new GameItem(x, y));
                        this.Model.EnemiesGiven = true;
                    }

                    if (lines[y + 2][x] == 'v')
                    {
                        this.Model.VerticalEnemies.Add(new GameItem(x, y));
                        this.Model.EnemiesGiven = true;
                    }

                    if (lines[y + 2][x] == 'f')
                    {
                        this.Model.FollowerEnemies.Add(new GameItem(x, y));
                        this.Model.EnemiesGiven = true;
                    }
                }
            }
        }

        /// <summary>
        /// delegate for the event for the collision of gameitems
        /// </summary>
        /// <param name="source">gets the other gameitem with which it may collide as a parameters</param>
        public delegate void EnemyHitDelegate(GameItem source);

        /// <summary>
        /// event for the collision with an enemy gameitem
        /// </summary>
        public event EnemyHitDelegate EnemyHit;

        /// <summary>
        /// Gets or sets counter responsible for counting the transition from one level to an other
        /// </summary>
        public int LevelCounter { get; set; }

        /// <summary>
        /// Gets the Model property
        /// </summary>
        public GameModel Model { get; }

        /// <summary>
        /// Method responsible for moving the mouse gameitem
        /// </summary>
        /// <param name="dx">gets a speed parameter in the x direction</param>
        /// <param name="dy">gets a speed parameter in the y direction</param>
        /// <returns>true if the target is reached, otherwise false</returns>
        public bool MouseMove(int dx, int dy)
        {
            int newx = (int)this.Model.Mouse.X + dx;
            int newy = (int)this.Model.Mouse.Y + dy;

            if (newx >= 0 && newx < this.Model.Walls.GetLength(0) && newy >= 0 && newy < this.Model.Walls.GetLength(1) && !this.Model.Walls[newx, newy])
            {
                this.Model.Mouse = new Point(newx, newy);
            }

            // Check for collision with enemies
            this.CheckForEnemyHit();

            // Returns true if the target point has been reached
            return this.Model.Mouse.Equals(this.Model.Cheese);
        }

        /// <summary>
        /// Method responsible for the operation of the mousetrap
        /// </summary>
        /// <returns>true if the mouse got catched, otherwise false</returns>
        public bool CatchedMouse()
        {
            // check the position of the mouse
            int mousex = Convert.ToInt32(this.Model.Mouse.X);
            int mousey = Convert.ToInt32(this.Model.Mouse.Y);

            // if the mouse "got catched" --> return true, and this is the end of the game for him
            if (this.Model.MouseCatchers[mousex, mousey] == true)
            {
                return true;
            }

            // otherwise --> false, and the game continues
            return false;
        }

        /// <summary>
        /// Method responsible for iterating, thus moving each enemy gameitem
        /// </summary>
        public void MoveEnemies()
        {
            foreach (GameItem enemy in this.Model.VerticalEnemies)
            {
                this.MoveVerticalEnemy(enemy);
            }

            foreach (GameItem enemy in this.Model.HorizontalEnemies)
            {
                this.MoveHorizontalEnemy(enemy);
            }

            foreach (GameItem enemy in this.Model.FollowerEnemies)
            {
                this.MoveFollowerEnemy(enemy);
            }

            foreach (GameItem enemy in this.Model.BouncingEnemies)
            {
                this.MoveBouncingEnemy(enemy);
            }

            this.Model.Refresh();

            this.CheckForEnemyHit();
        }

        /// <summary>
        /// Method describing the movement of the enemy that capable to move horizontally
        /// </summary>
        /// <param name="enemy">gets the movable gameitem as parameter</param>
        private void MoveHorizontalEnemy(GameItem enemy)
        {
            int tempnewx = enemy.X + enemy.Dx;

            if (tempnewx <= 1 || tempnewx >= this.Model.Walls.GetLength(0) - 2 || this.Model.Walls[tempnewx, enemy.Y] || this.Model.MouseCatchers[tempnewx, enemy.Y])
            {
                enemy.Dx = -enemy.Dx;
            }

            int newx = enemy.X + enemy.Dx;

            enemy.X = newx;
        }

        /// <summary>
        /// Method describing the movement of the enemy that capable to move vertically
        /// </summary>
        /// <param name="enemy">gets the movable gameitem as parameter</param>
        private void MoveVerticalEnemy(GameItem enemy)
        {
            int newy = enemy.Y + enemy.Dy;

            if (newy <= 1 || newy >= this.Model.Walls.GetLength(1) - 2 || this.Model.Walls[enemy.X, newy] || this.Model.MouseCatchers[enemy.X, newy])
            {
                enemy.Dy = -enemy.Dy;
            }

            int ny = enemy.Y + enemy.Dy;

            enemy.Y = ny;
        }

        /// <summary>
        /// Method describing the movement of the enemy that capable to follow the user's gameitem
        /// </summary>
        /// <param name="enemy">gets the movable gameitem as parameter</param>
        private void MoveFollowerEnemy(GameItem enemy)
        {
            // target position
            double targetx = this.Model.Mouse.X;
            double targety = this.Model.Mouse.Y;

            // the position of the cat
            double orx = enemy.X;
            double ory = enemy.Y;

            // calculate the difference-- > velocity of the follower cat
            double diffx = targetx - orx;
            double diffy = targety - ory;

            int speedx;
            int speedy;

            // the cat would arrive immed. -- > divide to slow it down
            if (diffx < 0)
            {
                speedx = Convert.ToInt32(Math.Floor(diffx / 4));
                speedy = Convert.ToInt32(Math.Floor(diffy / 4));
            }
            else
            {
                speedx = Convert.ToInt32(Math.Ceiling(diffx / 4));
                speedy = Convert.ToInt32(Math.Ceiling(diffy / 4));
            }

            // to create the new position of the item
            int newx = enemy.X + speedx;
            int newy = enemy.Y + speedy;

            enemy.X = newx;
            enemy.Y = newy;
        }

        /// <summary>
        /// Method describing the movement of the enemy that has a bouncing movement
        /// </summary>
        /// <param name="enemy">gets the movable gameitem as parameter</param>
        private void MoveBouncingEnemy(GameItem enemy)
        {
            int newx = enemy.X + enemy.Dx;
            int newy = enemy.Y + enemy.Dy;

            if (!this.Model.Walls[newx, newy] && !this.Model.MouseCatchers[newx, newy])
            {
                enemy.X = newx;
                enemy.Y = newy;
                return;
            }

            if (!this.Model.Walls[newx, enemy.Y] && !this.Model.MouseCatchers[newx, enemy.Y])
            {
                enemy.Dy = -enemy.Dy;
                newy = enemy.Y + enemy.Dy;

                enemy.X = newx;

                if (!this.Model.Walls[newx, newy] && !this.Model.MouseCatchers[newx, newy])
                {
                    enemy.Y = newy;
                }

                return;
            }

            if (!this.Model.Walls[enemy.X, newy] && !this.Model.MouseCatchers[enemy.X, newy])
            {
                enemy.Dx = -enemy.Dx;
                newx = enemy.X + enemy.Dx;

                enemy.Y = newy;

                if (!this.Model.Walls[newx, newy] && !this.Model.MouseCatchers[newx, newy])
                {
                    enemy.X = newx;
                }

                return;
            }

            enemy.Dx = -enemy.Dx;
            enemy.Dy = -enemy.Dy;

            enemy.X += enemy.Dx;
            enemy.Y += enemy.Dy;
        }

        /// <summary>
        /// Method describing when to raise the event
        /// </summary>
        private void CheckForEnemyHit()
        {
            foreach (GameItem enemy in this.Model.VerticalEnemies)
            {
                if (this.Model.Mouse.X == enemy.X && this.Model.Mouse.Y == enemy.Y)
                {
                    this.EnemyHit?.Invoke(enemy);
                    return;
                }
            }

            foreach (GameItem enemy in this.Model.HorizontalEnemies)
            {
                if (this.Model.Mouse.X == enemy.X && this.Model.Mouse.Y == enemy.Y)
                {
                    this.EnemyHit?.Invoke(enemy);
                    return;
                }
            }

            foreach (GameItem enemy in this.Model.FollowerEnemies)
            {
                if (this.Model.Mouse.X == enemy.X && this.Model.Mouse.Y == enemy.Y)
                {
                    this.EnemyHit?.Invoke(enemy);
                    return;
                }
            }

            foreach (GameItem enemy in this.Model.BouncingEnemies)
            {
                if (this.Model.Mouse.X == enemy.X && this.Model.Mouse.Y == enemy.Y)
                {
                    this.EnemyHit?.Invoke(enemy);
                    return;
                }
            }
        }
    }
}
