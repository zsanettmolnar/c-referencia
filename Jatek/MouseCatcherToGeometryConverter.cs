﻿// <copyright file="MouseCatcherToGeometryConverter.cs" company="OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Jatek
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Converter to convert the mousetraps
    /// </summary>
    internal class MouseCatcherToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// Convert from gameitem to geometry
        /// </summary>
        /// <param name="value">bool array of mousetraps</param>
        /// <param name="targetType">GeometryGroup of rectangleGeometries</param>
        /// <param name="parameter">Converter parameter to use</param>
        /// <param name="culture">Culture to use</param>
        /// <returns>GeometryGroup</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool[,] mouseCatchers = value as bool[,];
            if (mouseCatchers == null)
            {
                return null;
            }

            GeometryGroup g = new GeometryGroup();
            for (int x = 0; x < mouseCatchers.GetLength(0); x++)
            {
                for (int y = 0; y < mouseCatchers.GetLength(1); y++)
                {
                    if (mouseCatchers[x, y] == true)
                    {
                        Rect r = new Rect(x * Constants.TILEW, y * Constants.TILEH, Constants.TILEW, Constants.TILEH);
                        g.Children.Add(new RectangleGeometry(r));
                    }
                }
            }

            return g;
        }

        /// <summary>
        /// No conversion back here
        /// </summary>
        /// <param name="value">GeometryGroup</param>
        /// <param name="targetType">Bool array of moustraps</param>
        /// <param name="parameter">Converter parameter to use</param>
        /// <param name="culture">Culture to use</param>
        /// <returns>Bool array</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}