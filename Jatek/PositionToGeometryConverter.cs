﻿// <copyright file="PositionToGeometryConverter.cs" company="OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Jatek
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Converter for convert point gameitems (cheese, mouse) to geoometry
    /// </summary>
    internal class PositionToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// Convert from Point to Geometry
        /// </summary>
        /// <param name="value">Point</param>
        /// <param name="targetType">Geometry</param>
        /// <param name="parameter">Converter parameter to use</param>
        /// <param name="culture">Culture to use</param>
        /// <returns>RectangleGeometry</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Point p = (Point)value; // i have to you explicit.
            if (p.X == 0 && p.Y == 0)
            {
                return null;
            }
            else
            {
                Rect r = new Rect(p.X * Constants.TILEW, p.Y * Constants.TILEH, Constants.TILEW, Constants.TILEH);
                return new RectangleGeometry(r);
            }
        }

        /// <summary>
        /// No conversion back here
        /// </summary>
        /// <param name="value">Geometry</param>
        /// <param name="targetType">Point</param>
        /// <param name="parameter">Converter parameter to use</param>
        /// <param name="culture">Culture  to use</param>
        /// <returns>Point, position of the gameitem</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
