﻿// <copyright file="GameModel.cs" company="OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Jatek
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Class of every displayable element
    /// </summary>
    internal class GameModel : Bindable
    {
        /// <summary>
        /// Bool array to store the position of the walls. True if there is a wall, otherwise false
        /// </summary>
        private bool[,] walls;

        /// <summary>
        /// Bool array to store the position of the mousetraps. Tru if there is one, otherwise false
        /// </summary>
        private bool[,] mouseCatchers;

        /// <summary>
        /// Private variable to store the position of the mouse gameitem
        /// </summary>
        private Point mouse;

        /// <summary>
        /// Private variable to store the position of the cheese gameitem
        /// </summary>
        private Point cheese;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// Create the list of the enemies
        /// </summary>
        public GameModel()
        {
            this.VerticalEnemies = new ObservableCollection<GameItem>();
            this.HorizontalEnemies = new ObservableCollection<GameItem>();
            this.FollowerEnemies = new ObservableCollection<GameItem>();
            this.BouncingEnemies = new ObservableCollection<GameItem>();
        }

        /// <summary>
        /// Gets or sets LevelNumber, property to store the levelnumber in a property
        /// </summary>
        public int LevelNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an enemy exist or not
        /// </summary>
        public bool EnemiesGiven { get; set; }

        /// <summary>
        /// Gets the list of enemies capable of moving vertically
        /// </summary>
        public ObservableCollection<GameItem> VerticalEnemies { get; }

        /// <summary>
        /// Gets the list of enemies capable of moving horizontally
        /// </summary>
        public ObservableCollection<GameItem> HorizontalEnemies { get; }

        /// <summary>
        /// Gets the list of enemies capable to follow the user's gameitem
        /// </summary>
        public ObservableCollection<GameItem> FollowerEnemies { get; }

        /// <summary>
        /// Gets the list of enemies having bouncing movement
        /// </summary>
        public ObservableCollection<GameItem> BouncingEnemies { get; }

        /// <summary>
        /// Gets a value for refreshing itself
        /// </summary>
        public GameModel Self
        {
            get
            {
                return this;
            }
        }

        /// <summary>
        /// Gets or sets the bool array of positions of the wall items
        /// </summary>
        public bool[,] Walls
        {
            get
            {
                return this.walls;
            }

            set
            {
                this.SetProperty(ref this.walls, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets the value of the mouse's position
        /// </summary>
        public Point Mouse
        {
            get
            {
                return this.mouse;
            }

            set
            {
                this.SetProperty(ref this.mouse, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets the value of the cheese's position
        /// </summary>
        public Point Cheese
        {
            get
            {
                return this.cheese;
            }

            set
            {
                this.SetProperty(ref this.cheese, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets or sets the bool array of positions of the mousetraps
        /// </summary>
        public bool[,] MouseCatchers
        {
            get
            {
                return this.mouseCatchers;
            }

            set
            {
                this.SetProperty(ref this.mouseCatchers, value);
                this.OnPropertyChanged(nameof(this.Self));
            }
        }

        /// <summary>
        /// Gets the appropriate brush for the mouse
        /// </summary>
        public Brush MouseBrush
        {
            get
            {
                return this.GetBrush("Images/_jerry.png");
            }
        }

        /// <summary>
        /// Gets the appropriate brush for the enemy having horizontal movement
        /// </summary>
        public Brush HorizontalEnemyBrush
        {
            get
            {
                return this.GetBrush("Images/cat.png");
            }
        }

        /// <summary>
        /// Gets the appropriate brush for the cheese
        /// </summary>
        public Brush CheeseBrush
        {
            get
            {
                return this.GetBrush("Images/targetcheese.png");
            }
        }

        /// <summary>
        /// Gets the appropriate brush for the wall
        /// </summary>
        public Brush WallBrush
        {
            get
            {
                return this.GetBrush("Images/_wall.bmp");
            }
        }

        /// <summary>
        /// Gets the appropriate brush for the enemy having vertical movement
        /// </summary>
        public Brush VerticalEnemyBrush
        {
            get
            {
                return this.GetBrush("Images/dog.png");
            }
        }

        /// <summary>
        /// Gets the appropriate brush for the follower enemy
        /// </summary>
        public Brush FollowerEnemyBrush
        {
            get
            {
                return this.GetBrush("Images/bear.png");
            }
        }

        /// <summary>
        /// Gets the appropriate brush for the bouncing enemy
        /// </summary>
        public Brush BouncingEnemyBrush
        {
            get
            {
                return this.GetBrush("Images/lion.png");
            }
        }

        /// <summary>
        /// Gets the appropriate brush for the mousetrap
        /// </summary>
        public Brush MouseCatchersBrush
        {
            get
            {
                return this.GetBrush("Images/egerfogo.png");
            }
        }

        /// <summary>
        /// Method responsible for the refresh of the observablelist of enemies, if there is a content change
        /// </summary>
        public void Refresh()
        {
            this.OnPropertyChanged(nameof(this.VerticalEnemies));
            this.OnPropertyChanged(nameof(this.HorizontalEnemies));
            this.OnPropertyChanged(nameof(this.FollowerEnemies));
            this.OnPropertyChanged(nameof(this.BouncingEnemies));
        }

        /// <summary>
        /// Method for setting the image brush
        /// </summary>
        /// <param name="fname">the filename of the appripriate image brush</param>
        /// <returns>ImageBrush</returns>
        private Brush GetBrush(string fname)
        {
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(fname, UriKind.Relative)));

            ib.TileMode = TileMode.Tile;
            ib.Viewport = new Rect(0, 0, Constants.TILEW, Constants.TILEH);
            ib.ViewportUnits = BrushMappingMode.Absolute;

            return ib;
        }
    }
}