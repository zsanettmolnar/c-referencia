﻿// <copyright file="Bindable.cs" company="OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Jatek
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Responsible class for data binding
    /// </summary>
    public class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// Event declared for binding, raised when data has changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raise the event if the property has changed
        /// </summary>
        /// <param name="name">name of the property</param>
        protected void OnPropertyChanged(string name = null)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Set the newvalue
        /// </summary>
        /// <typeparam name="T">generic type</typeparam>
        /// <param name="field">the changed value</param>
        /// <param name="newvalue">the newvalue to be inserted</param>
        /// <param name="name">name of the property</param>
        protected void SetProperty<T>(ref T field, T newvalue, [CallerMemberName] string name = null)
        {
            field = newvalue;
            this.OnPropertyChanged(name);
        }
    }
}
