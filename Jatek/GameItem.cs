﻿// <copyright file="GameItem.cs" company="OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Jatek
{
    /// <summary>
    /// Class containing the common properties of every gameitem
    /// </summary>
    internal class GameItem : Bindable
    {
        /// <summary>
        /// private field to store the value of the property X
        /// </summary>
        private int x;

        /// <summary>
        /// private field to store the value of the  property y
        /// </summary>
        private int y;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameItem"/> class.
        /// </summary>
        /// <param name="x">x position coordinate of the gameitem</param>
        /// <param name="y">y position coordinate of the gameitem</param>
        public GameItem(int x, int y)
        {
            this.X = x;
            this.Y = y;

            this.Dx = 1;
            this.Dy = 1;
        }

        /// <summary>
        /// Gets or sets property X
        /// </summary>
        public int X
        {
            get
            {
                return this.x;
            }

            set
            {
                this.SetProperty(ref this.x, value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets property Y
        /// </summary>
        public int Y
        {
            get
            {
                return this.y;
            }

            set
            {
                this.SetProperty(ref this.y, value);
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets property Dx is responsible for the speed of the gameitem in the x direction
        /// </summary>
        public int Dx { get; set; }

        /// <summary>
        /// Gets or sets property Dy is responsible for the speed of the gameitem in the y direction
        /// </summary>
        public int Dy { get; set; }
    }
}