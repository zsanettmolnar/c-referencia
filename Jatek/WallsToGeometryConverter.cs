﻿// <copyright file="WallsToGeometryConverter.cs" company="OE">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Jatek
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Class for converting the wall gameItem to geometry
    /// </summary>
    internal class WallsToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// Converting from bool array to geometry
        /// </summary>
        /// <param name="value">bool array</param>
        /// <param name="targetType">GeometryGroup</param>
        /// <param name="parameter">Converter parameter to use</param>
        /// <param name="culture">Culture to use</param>
        /// <returns>GeometryGroup of RectangleGeometries</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool[,] walls = value as bool[,];
            if (walls == null)
            {
                return null;
            }

            GeometryGroup g = new GeometryGroup();
            for (int x = 0; x < walls.GetLength(0); x++)
            {
                for (int y = 0; y < walls.GetLength(1); y++)
                {
                    if (walls[x, y] == true)
                    {
                        Rect r = new Rect(x * Constants.TILEW, y * Constants.TILEH, Constants.TILEW, Constants.TILEH);
                        g.Children.Add(new RectangleGeometry(r));
                    }
                }
            }

            return g;
        }

        /// <summary>
        /// No conversion back here
        /// </summary>
        /// <param name="value">GeometryGroup</param>
        /// <param name="targetType">Bool array</param>
        /// <param name="parameter">Converter parameter to use</param>
        /// <param name="culture">Culture to use</param>
        /// <returns>Bool array of positions</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}